# Dockerfile
#

FROM dockerfile/java

# newrelic (optional)
#
# RUN echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list
# RUN wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -
# RUN apt-get update
# RUN apt-get install newrelic-sysmond
# RUN nrsysmond-config --set license_key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # <-- set your newrelic license key below
# RUN /etc/init.d/newrelic-sysmond start


# setup the app
# 
ADD /.ebextensions /.ebextensions
ADD /target /target
ADD /src /src

# set any environment vars
# 
RUN export ENV_VAR_XXX=production

CMD ["java", "-jar", "/target/server-standalone.jar", "8081"]

## example cmd if you are using the newrelic agent
#
#CMD ["java", "-javaagent:/target/newrelic.jar", "-Dapp.env=production", "-jar", "/target/server-standalone.jar", "8081"]

EXPOSE 8081
