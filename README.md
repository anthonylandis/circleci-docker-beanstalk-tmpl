# Dockerize and deploy a clojure app from circleci

These files can be copied to the root of your existing clojure project.

`circleci.yml` is the config for circleci. You may want to update how lein runs your tests.

`docker.sh` is called in the deploy loop at circlci. You will need to set your s3 bucket to push up the zipped up app and add any resources in your project to the zip file that may be needed.

You will need to setup aws keys in your circleci account so awscli can push to your s3 bucket as well as call the beanstalk api.

`Dockerfile` includes commented out examples to install newrelic server and app monitoring.

