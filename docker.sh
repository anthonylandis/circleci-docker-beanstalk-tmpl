#! /bin/bash

## Update DOCKER_S3 to your s3 bucket for the docker .zip file
#
DOCKER_S3=my-docker-files-bucket
SHA1=$1
WHO=$2
CIRCLE_COMPARE_URL=$3
DOCKER_VER=$SHA1-$APP_NAME
DOCKER_ZIP=$SHA1-Docker.zip

yes | lein upgrade
cp -r resources/* src/public/
lein uberjar

## UPDATE DOCKER FILE
#
sed -i 's/%DEPLOYMENT_ID%/$SHA1/g' Dockerfile

## CREATE THE ARCHIVE TO SHIP (include any other jars or resources in the zip file as needed)
#
zip $DOCKER_ZIP -r Dockerrun.aws.json Dockerfile target/server-standalone.jar src/logback.xml .ebextensions/ 


## DEPLOY TO THE WEST
#
REGION=us-west-2
ENV_NAME=myBeanstalk-env   # change this
APP_NAME=my-app            # change this

aws s3 cp $DOCKER_ZIP s3://$DOCKER_S3/$DOCKER_ZIP
aws elasticbeanstalk create-application-version \
	--region $REGION \
	--application-name $APP_NAME \
	--version-label $DOCKER_VER \
	--source-bundle S3Bucket=$DOCKER_S3,S3Key=$DOCKER_ZIP
aws elasticbeanstalk update-environment \
	--environment-name $ENV_NAME \
	--version-label $DOCKER_VER \
	--region $REGION



## DEPLOY TO THE EAST
#

REGION=us-east-1
ENV_NAME=myBeanstalk-env    # change this
APP_NAME=my-app             # change this

aws s3 cp $DOCKER_ZIP s3://$DOCKER_S3/$DOCKER_ZIP
aws elasticbeanstalk create-application-version \
	--region $REGION \
	--application-name $APP_NAME \
	--version-label $DOCKER_VER \
	--source-bundle S3Bucket=$DOCKER_S3,S3Key=$DOCKER_ZIP
aws elasticbeanstalk update-environment \
	--environment-name $ENV_NAME \
	--version-label $DOCKER_VER \
	--region $REGION


## New Relic Deployment (update the the api-key)
#
curl -H "x-api-key:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" \ 
	-d "deployment[application_id]=6766919" \
	-d "deployment[revision]=$SHA" \
	-d "deployment[user]=$WHO" \
	-d "deployment[changelog]=$CIRCLE_COMPARE_URL" \
        -d "deployment[description]=This deployment was sent from circleci" \
	https://api.newrelic.com/deployments.xml
